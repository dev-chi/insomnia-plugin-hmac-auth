const CryptoJS = require("crypto-js");
const Base64   = require('crypto-js/enc-base64');
const hmacSHA1 = require('crypto-js/hmac-sha1');
const jsSHA    = require('jssha')

module.exports = function(context) {

  const request = context.request;

  const app_id   = request.getEnvironmentVariable("app_id");
  const app_key = request.getEnvironmentVariable("app_key");

  if(app_id === undefined || app_key === undefined) {
    return;
  }

  const xDate = (new Date()).toGMTString();

  // const signature    = hmacSHA1('SHA-1', app_key).toString(Base64);
  const hash    = new jsSHA('SHA-1', 'TEXT');
  hash.setHMACKey(app_key, 'TEXT');
  hash.update('x-date: ' + xDate);
  const signature = hash.getHMAC('B64')
  const authToken    = 'hmac username=\"' + app_id + '\", algorithm=\"hmac-sha1\", headers=\"x-date\", signature=\"' + signature + '\"';

  context.request.setHeader('x-date', xDate);
  context.request.setHeader('Authorization', authToken);
};
